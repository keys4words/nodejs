# gitlab practice with nodejs

simple nodejs app for practice with gitlab

using 2 own runners:
- for tests
- for build && deploy

3 stages:
- tests
- build && push to gitlab Registry
- deploy to dev server
